### What is this repository for? ###

* This repository stores the code of RNAseq pipeline on polly. We have four main notebooks. two notebooks are for upstream analysis (one for if user has raw fastq files available on polly other one if data needs to be downloaded from websource (ENA).
* other two notebooks are for report generation (Downstream analysis) one notebook is specific to Cygnal while other one is for general usage. The docker folder contains the dockerfile, some json files for testing and other utility files like invoke.sh and extra packages.
* Version 4
* For explained tutorial refer [Docs](https://elucidatainc.atlassian.net/wiki/spaces/~274679770/pages/2927263745/RNAseq+pipeline+on+polly+-+Cygnal)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* kartik.mishra@elucidata.io
* kriti@elucidata.io